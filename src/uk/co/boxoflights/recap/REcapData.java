package uk.co.boxoflights.recap;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import uk.co.boxoflights.recap.util.FileHelper;

import android.content.Context;

public class REcapData {
	
	private static Context appContext = null;
	private static ArrayList<Item> items = null;
	private static boolean initialised = false;
	
	public static void initialise(Context context) {
		if(!initialised) {
			appContext = context;
			
			String file = FileHelper.readFromInternalFile("datastore-json.txt", appContext);
			
			if(file != null && file != "") {
				Gson gson = new Gson();
				Type itemsType = new TypeToken<ArrayList<Item>>(){}.getType();
				try {
					ArrayList<Item> loadedItems = gson.fromJson(file, itemsType);
					if(loadedItems != null) {
						items = loadedItems;
					}
				} catch (JsonSyntaxException e) {
					e.printStackTrace();
				}
			}
			
			if(items == null) {
				items = new ArrayList<Item>();
			}
			
			initialised = true;
			
		}
	}
	
	public static boolean isInitialised() {
		return initialised;
	}
	
	public static ArrayList<Item> getItems(){
		return items;
	}
	
	public static int addItem(Item item) {
		if(initialised && items != null) {
			int id = items.size();
			items.add(item);
			return id;
		}
		return -1;
	}
	
	public static Item getItem(int id) {
		if(initialised && items != null && !items.isEmpty()) {
			if(id < items.size() && id >= 0) {
				return items.get(id);
			}
		}
		return null;
	}
	
	public static void saveData() {
		if(initialised && items != null && !items.isEmpty()) {
			Gson gson = new Gson();
			String json = gson.toJson(items);
			FileHelper.saveToInternalFile(json, "datastore-json.txt", appContext);
		}
    }
}
