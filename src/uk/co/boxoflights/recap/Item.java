package uk.co.boxoflights.recap;

import java.io.Serializable;
import java.util.ArrayList;


public class Item {
	
	ArrayList<DetailGroup> detailGroups;
	String location;
	
	public Item() {
		detailGroups = new ArrayList<DetailGroup>();
		location = "";
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ArrayList<DetailGroup> getDetailGroups() {
		return detailGroups;
	}

	public void setDetailGroups(ArrayList<DetailGroup> detailGroups) {
		this.detailGroups = detailGroups;
	}

	public void addDetailGroup(DetailGroup group) {
		this.detailGroups.add(group);
	}
	
	@Override
	public String toString() {
		String s = "--------------------------------------\n" +
				"Location: " + location + "\n" +
				"DetailGroups: \n";
		
		DetailGroup dg = null;
		ArrayList<String> tags = null;
		ArrayList<Detail> details = null;
		Detail dtl = null;
		
		int totalGroups = detailGroups.size();
		for(int g = 0; g < totalGroups; g++) {
			
			dg = detailGroups.get(g);
			
			s += "    Group " + g + ": \n" +
					"        Tags: \n";
			
			tags = dg.getTags();
			int totalTags = tags.size();
			for(int t = 0; t < totalTags; t++) {
				s += "            Tag " + t + ": " + tags.get(t) + "\n";
			}
			
			s += "        Details: \n";
			
			details = dg.getDetails();
			int totalDetails = details.size();
			for(int d = 0; d < totalDetails; d++) {
				dtl = details.get(d);
				s += "            Detail " + d + ": \n" +
						"                Text: " + dtl.getText() + "\n" +
						"                Info: " + dtl.getInfo() + "\n" +
						"                Correct: " + dtl.isCorrect() + "\n";
			}
		}
		s += "--------------------------------------\n";
		return s;
	}
}
