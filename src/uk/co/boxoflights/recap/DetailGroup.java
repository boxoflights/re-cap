package uk.co.boxoflights.recap;

import java.io.Serializable;
import java.util.ArrayList;

public class DetailGroup {
	ArrayList<Detail> details;
	ArrayList<String> tags;
	
	public DetailGroup() {
		this.details = new ArrayList<Detail>(4);
		this.tags = new ArrayList<String>();
	}

	public ArrayList<Detail> getDetails() {
		return details;
	}

	public void setDetails(ArrayList<Detail> details) {
		this.details = details;
	}

	public void addDetail(Detail detail) {
		this.details.add(detail);
	}
	
	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	public void addTag(String tag) {
		if(!tags.contains(tag)) {
			this.tags.add(tag);
		}
	}

	public Detail getDetail(int id) {
		return details.get(id);
	}
	
}
