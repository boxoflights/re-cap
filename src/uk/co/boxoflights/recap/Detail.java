package uk.co.boxoflights.recap;

import java.io.Serializable;

public class Detail {
	
	String text;
	String info;
	
	boolean correct;
	
	public Detail() {
		text = "";
		info = "";
		correct = false;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
		
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public boolean isCorrect() {
		return correct;
	}
	
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
}
