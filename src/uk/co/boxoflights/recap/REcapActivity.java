package uk.co.boxoflights.recap;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import uk.co.boxoflights.recap.activities.AddDetailGroupsActivity;
import uk.co.boxoflights.recap.activities.AddImageActivity;
import uk.co.boxoflights.recap.activities.GameplayActivity;
import uk.co.boxoflights.recap.util.FileHelper;

import com.google.gson.Gson;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class REcapActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        REcapData.initialise(getApplicationContext());
        
        setContentView(R.layout.activity_recap);
        
        Button btnPlay = (Button) findViewById(R.id.btnPlay);
		if(btnPlay != null) {
			btnPlay.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(REcapActivity.this, GameplayActivity.class);     
			        startActivity(i);
				}
			});
		}
		
		Button btnAddImage = (Button) findViewById(R.id.addImage);
		if(btnAddImage != null) {
			btnAddImage.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(REcapActivity.this, AddImageActivity.class);     
			        startActivity(i);
				}
			});
		}
        
    }
    
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_recap, menu);
        return true;
    }
    
}
