package uk.co.boxoflights.recap.activities;

import java.util.ArrayList;
import java.util.Random;

import uk.co.boxoflights.recap.Detail;
import uk.co.boxoflights.recap.DetailGroup;
import uk.co.boxoflights.recap.Item;
import uk.co.boxoflights.recap.R;
import uk.co.boxoflights.recap.REcapData;
import uk.co.boxoflights.recap.R.layout;
import uk.co.boxoflights.recap.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.widget.Button;
import android.widget.ImageView;

public class GameplayActivity extends Activity {
	
	ArrayList<Item> items;
	int totalItems;
	
	int currentItemId;
	Item currentItem;
	
	ArrayList<DetailGroup> currentDetailGroups;
	int currentDetailGroupId;
	DetailGroup currentDetailGroup;
	
	Detail[] currentDetails;
	
	Random rnd;
	
	private Button m1,m2,m3,m4;
	private ImageView imageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		rnd = new Random();
		items = REcapData.getItems();
		
		if(items == null || items.isEmpty()) {
			
			Item it = null;
			ArrayList<DetailGroup> dgs = null;
			DetailGroup dg = null;
			Detail dt = null;
			
			for(int t = 0; t < 20; t++) {
				
				it = new Item();
				it.setLocation("images/image" + t + ".png");
				
				dgs = new ArrayList<DetailGroup>();
				
				for(int dgn = 0; dgn < 2; dgn++) {
					
					dg = new DetailGroup();
					
					dg.addTag("tag1");
					dg.addTag("tag2");
					
					for(int d = 0; d < 4; d++) {
						
						dt = new Detail();
						
						dt.setCorrect(true);
						dt.setText("image " + t + " group " + dgn + " detail " + d);
						dt.setInfo("some info");
						dg.addDetail(dt);
						
					}
					
					dgs.add(dg);
				}
				
				it.setDetailGroups(dgs);
				items.add(it);
			}
			
		}
		
		totalItems = items.size();
		
		setContentView(R.layout.activity_gameplay);
		
		if(totalItems > 0) {
			currentItemId = rnd.nextInt(totalItems);
			currentItem = items.get(currentItemId);
		
			currentDetailGroups = currentItem.getDetailGroups();
		
			currentDetailGroupId = 0;
			currentDetailGroup = currentDetailGroups.get(currentDetailGroupId);
		
			currentDetails = new Detail[] {
				currentDetailGroup.getDetail(0),
				currentDetailGroup.getDetail(1),
				currentDetailGroup.getDetail(2),
				currentDetailGroup.getDetail(3)
			};
		
			m1 = (Button)findViewById(R.id.btnMemory1);
			m1.setText(currentDetails[0].getText());
		
			m2 = (Button)findViewById(R.id.btnMemory2);
			m2.setText(currentDetails[1].getText());
		
			m3 = (Button)findViewById(R.id.btnMemory3);
			m3.setText(currentDetails[2].getText());
		
			m4 = (Button)findViewById(R.id.btnMemory4);
			m4.setText(currentDetails[3].getText());
		
			imageView = (ImageView)findViewById(R.id.ivImage);
			imageView.setImageBitmap(BitmapFactory.decodeFile(currentItem.getLocation()));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_gameplay, menu);
		return true;
	}

}
