package uk.co.boxoflights.recap.activities;

import uk.co.boxoflights.recap.DetailGroup;
import uk.co.boxoflights.recap.Item;
import uk.co.boxoflights.recap.R;
import uk.co.boxoflights.recap.REcapActivity;
import uk.co.boxoflights.recap.REcapData;
import uk.co.boxoflights.recap.R.layout;
import uk.co.boxoflights.recap.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AddCompletedActivity extends Activity {

	private int itemId;
	private Item item;
	private ImageView imageView;
	
	Button btnOk;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		itemId = getIntent().getExtras().getInt("itemId");
		item = REcapData.getItem(itemId);
		
		setContentView(R.layout.activity_add_completed);

		imageView = (ImageView) findViewById(R.id.ivImagePreview);
		imageView.setImageBitmap(BitmapFactory.decodeFile(item.getLocation()));
		
		btnOk = (Button)findViewById(R.id.btnOK);
		btnOk.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				REcapData.saveData();
				Intent i = new Intent(AddCompletedActivity.this, REcapActivity.class);
				startActivity(i);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_completed, menu);
		return true;
	}

}
