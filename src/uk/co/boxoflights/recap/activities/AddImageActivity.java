package uk.co.boxoflights.recap.activities;

import uk.co.boxoflights.recap.Item;
import uk.co.boxoflights.recap.R;
import uk.co.boxoflights.recap.REcapData;
import uk.co.boxoflights.recap.R.id;
import uk.co.boxoflights.recap.R.layout;
import uk.co.boxoflights.recap.R.menu;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AddImageActivity extends Activity {

	final int SELECT_IMAGE = 1;
	Item item;
	Button btnNext;
	ImageView imageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		item = new Item();
		//Create new image Item object to work with
		setContentView(R.layout.activity_add_image);
		
		Button btnChooseImage = (Button) findViewById(R.id.btnChooseImage);
		if(btnChooseImage != null) {
			btnChooseImage.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(i, SELECT_IMAGE);
				}
			});
		}
		
		btnNext = (Button) findViewById(R.id.btnNext);
		btnNext.setEnabled(false);
		if(btnNext != null) {
			btnNext.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(AddImageActivity.this, AddDetailGroupsActivity.class);
					i.putExtra("itemId", REcapData.addItem(item));
					startActivity(i);
					finish();
				}
			});
		}
		
		imageView = (ImageView) findViewById(R.id.ImagePreview);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            item.setLocation(cursor.getString(columnIndex));
            cursor.close();
            
            imageView.setImageBitmap(BitmapFactory.decodeFile(item.getLocation()));
            
            btnNext.setEnabled(true);
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_image, menu);
		return true;
	}

}
