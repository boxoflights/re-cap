package uk.co.boxoflights.recap.activities;

import uk.co.boxoflights.recap.Detail;
import uk.co.boxoflights.recap.DetailGroup;
import uk.co.boxoflights.recap.Item;
import uk.co.boxoflights.recap.R;
import uk.co.boxoflights.recap.R.layout;
import uk.co.boxoflights.recap.R.menu;
import uk.co.boxoflights.recap.REcapData;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.TabActivity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressLint("NewApi")
public class AddDetailGroupsActivity extends TabActivity {

	final int maxDetails = 4;
	
	int itemId;
	Item item;
	
	Detail[] details;
	
	EditText tvMemory1;
	EditText tvMemoryDesc1;
	
	EditText tvMemory2;
	EditText tvMemoryDesc2;
	
	EditText tvMemory3;
	EditText tvMemoryDesc3;
	
	EditText tvMemory4;
	EditText tvMemoryDesc4;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		itemId = getIntent().getExtras().getInt("itemId");
		item = REcapData.getItem(itemId);
		details = new Detail[maxDetails];
		for(int d = 0; d < maxDetails; d++) {
			details[d] = new Detail();
		}
		setContentView(R.layout.activity_add_detail_groups);
		
		tvMemory1 = (EditText)findViewById(R.id.etMemory1);
		tvMemoryDesc1 = (EditText)findViewById(R.id.etMemoryDescription1);
		
		tvMemory2 = (EditText)findViewById(R.id.etMemory2);
		tvMemoryDesc2 = (EditText)findViewById(R.id.etMemoryDescription2);
		
		tvMemory3 = (EditText)findViewById(R.id.etMemory3);
		tvMemoryDesc3 = (EditText)findViewById(R.id.etMemoryDescription3);
		
		tvMemory4 = (EditText)findViewById(R.id.etMemory4);
		tvMemoryDesc4 = (EditText)findViewById(R.id.etMemoryDescription4);
		
		Button btnNext = (Button) findViewById(R.id.btnNext);
		if(btnNext != null) {
			btnNext.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(AddDetailGroupsActivity.this, AddCompletedActivity.class);
					
					DetailGroup detailGroup = new DetailGroup();
					
					String[] text = new String[] {
							tvMemory1.getText().toString(),
							tvMemory2.getText().toString(),
							tvMemory3.getText().toString(),
							tvMemory4.getText().toString()
					};
					
					String[] info = new String[] {
							tvMemoryDesc1.getText().toString(),
							tvMemoryDesc2.getText().toString(),
							tvMemoryDesc3.getText().toString(),
							tvMemoryDesc4.getText().toString()
					};
					
					for(int d = 0; d < maxDetails; d++) {
						Detail dtl = details[d];
						dtl.setText(text[d]);
						dtl.setInfo(info[d]);
						detailGroup.addDetail(details[d]);
					}
					item.addDetailGroup(detailGroup);
					
					i.putExtra("itemId", itemId);
					startActivity(i);
					finish();
				}
			});
		}
		
		TabHost mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		
		TabSpec spec1 = mTabHost.newTabSpec("Memory 1");
		spec1.setContent(R.id.tabMemory1);
		spec1.setIndicator("Memory 1");
		mTabHost.addTab(spec1);
		
		TabSpec spec2 = mTabHost.newTabSpec("Memory 2");
		spec2.setContent(R.id.memoryTab2);
		spec2.setIndicator("Memory 2");
		mTabHost.addTab(spec2);
		
		TabSpec spec3 = mTabHost.newTabSpec("Memory 3");
		spec3.setContent(R.id.memoryTab3);
		spec3.setIndicator("Memory 3");
		mTabHost.addTab(spec3);
		
		TabSpec spec4 = mTabHost.newTabSpec("Memory 4");
		spec4.setContent(R.id.memoryTab4);
		spec4.setIndicator("Memory 4");
		mTabHost.addTab(spec4);
		
	}

}
