package uk.co.boxoflights.recap.activities;

import uk.co.boxoflights.recap.R;
import uk.co.boxoflights.recap.R.layout;
import uk.co.boxoflights.recap.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class SetTagsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_tags);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_set_tags, menu);
		return true;
	}

}
