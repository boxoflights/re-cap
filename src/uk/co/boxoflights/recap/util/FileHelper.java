package uk.co.boxoflights.recap.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;

public class FileHelper {
	public static String readFromInternalFile(String filename, Context appContext) {
        FileInputStream fis = null;
        String jsonString = "";
        
        try {
            fis = appContext.openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader buffreader = new BufferedReader(isr);
            String readString = buffreader.readLine() ;
            while(readString != null) {
            	jsonString = jsonString + readString ;
                readString = buffreader.readLine() ;
            }
            
            fis.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        return jsonString;
    }
    
	public static void saveToInternalFile(String data, String filename, Context appContext) {
        
        FileOutputStream fos = null;
        try {
            fos = appContext.openFileOutput(filename
                                    ,Context.MODE_PRIVATE);
            fos.write(data.getBytes());
            
            fos.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
